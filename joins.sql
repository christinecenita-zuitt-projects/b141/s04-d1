Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 53
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.092 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> SHOW TABLES
    -> ;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+-----------+
| id | name      |
+----+-----------+
|  1 | Rivermaya |
|  2 | Psy       |
+----+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE artists;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(50) | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
2 rows in set (0.140 sec)

MariaDB [music_db]> INSERT INTO artists (name)
    -> VALUES ("Taylor Swift"),
    -> ("Lady  Gaga"),
    -> ("Justin Bieber"),
    -> ("Ariana Grande"),
    -> ("Bruno Mars");
Query OK, 5 rows affected (0.216 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Rivermaya     |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady  Gaga    |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-------+------------+-----------+
| id | name  | year       | artist_id |
+----+-------+------------+-----------+
|  1 | Psy 6 | 2012-01-01 |         2 |
|  2 | Trip  | 1996-01-01 |         1 |
+----+-------+------------+-----------+
2 rows in set (0.026 sec)

MariaDB [music_db]> INSERT INTO albums (name, year, artist_id) VALUES
    -> ("Fearless", "2008-01-01", 3),
    -> ("Red", "2012-02-02", 3),
    -> ("A Star Is Born", "2018-03-03", 4),
    -> ("Born This Way", "2011-04-04", 4),
    -> ("Purpose", "2015-05-05", 5),
    -> ("Believe", "2012-06-06", 5),
    -> ("Dangerous Woman", "2016-07-07", 6),
    -> ("Thank U, Next", "2019-08-08", 6),
    -> ("24K Magic", "2016-09-09", 7),
    -> ("Earth to Mars", "2011-10-10", 7);
Query OK, 10 rows affected (0.279 sec)
Records: 10  Duplicates: 0  Warnings: 0

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
|  3 | Fearless        | 2008-01-01 |         3 |
|  4 | Red             | 2012-02-02 |         3 |
|  5 | A Star Is Born  | 2018-03-03 |         4 |
|  6 | Born This Way   | 2011-04-04 |         4 |
|  7 | Purpose         | 2015-05-05 |         5 |
|  8 | Believe         | 2012-06-06 |         5 |
|  9 | Dangerous Woman | 2016-07-07 |         6 |
| 10 | Thank U, Next   | 2019-08-08 |         6 |
| 11 | 24K Magic       | 2016-09-09 |         7 |
| 12 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

MariaDB [music_db]> DELETE FROM albums WHERE id >= 3;
Query OK, 10 rows affected (0.142 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-------+------------+-----------+
| id | name  | year       | artist_id |
+----+-------+------------+-----------+
|  1 | Psy 6 | 2012-01-01 |         2 |
|  2 | Trip  | 1996-01-01 |         1 |
+----+-------+------------+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-------+------------+-----------+
| id | name  | year       | artist_id |
+----+-------+------------+-----------+
|  1 | Psy 6 | 2012-01-01 |         2 |
|  2 | Trip  | 1996-01-01 |         1 |
+----+-------+------------+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> INSERT INTO albums (name, year, artist_id) VALUES
    -> ("Fearless", "2008-01-01", 3),
    -> ("Red", "2012-02-02", 3),
    -> ("A Star Is Born", "2018-03-03", 4),
    -> ("Born This Way", "2011-04-04", 4),
    -> ("Purpose", "2015-05-05", 5),
    -> ("Believe", "2012-06-06", 5),
    -> ("Dangerous Woman", "2016-07-07", 6),
    -> ("Thank U, Next", "2019-08-08", 6),
    -> ("24K Magic", "2016-09-09", 7),
    -> ("Earth to Mars", "2011-10-10", 7);
Query OK, 10 rows affected (0.113 sec)
Records: 10  Duplicates: 0  Warnings: 0

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.003 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Rivermaya     |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady  Gaga    |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 126
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> INSERT INTO songs (title, length, genre, album_id) VALUES
    -> ("Fearless", 246, "Pop rock", 13),
    -> ("Love Story", 213, "Country pop", 13),
    -> ("State of Grace", 213, "Alternative Rock", 14),
    -> ("Red", 204, "Country", 14),
    -> ("Black Eyes", 221, "Rock and roll", 15),
    -> ("Shallow", 201, "Country, rock, folk rock", 15),
    -> ("Born This Way", 252, "Electropop", 16),
    -> ("Sorry", 232, "Dancehall-poptropical housemoombahton", 17),
    -> ("Boyfriend", 251, "Pop", 18),
    -> ("Into You", 242, "EDM house", 19),
    -> ("Thank U Next", 236, "Pop, R&B", 20),
    -> ("24K Magic", 207, "Funk, disco, R&B", 21),
    -> ("Lost", 232, "Pop", 22);
Query OK, 13 rows affected (0.256 sec)
Records: 13  Duplicates: 0  Warnings: 0

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Rivermaya     |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady  Gaga    |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE id != 11;
+----+----------------+----------+--------------------------+----------+
| id | title          | length   | genre                    | album_id |
+----+----------------+----------+--------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                    |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                      |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                      |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                 |       13 |
|  5 | Love Story     | 00:02:13 | Country pop              |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock         |       14 |
|  7 | Red            | 00:02:04 | Country                  |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll            |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop               |       16 |
| 12 | Boyfriend      | 00:02:51 | Pop                      |       18 |
| 13 | Into You       | 00:02:42 | EDM house                |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                 |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B         |       21 |
| 16 | Lost           | 00:02:32 | Pop                      |       22 |
+----+----------------+----------+--------------------------+----------+
15 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE id < 9;
+----+----------------+----------+------------------+----------+
| id | title          | length   | genre            | album_id |
+----+----------------+----------+------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop            |        1 |
|  2 | Kundiman       | 00:02:40 | OPM              |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM              |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock         |       13 |
|  5 | Love Story     | 00:02:13 | Country pop      |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock |       14 |
|  7 | Red            | 00:02:04 | Country          |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll    |       15 |
+----+----------------+----------+------------------+----------+
8 rows in set (0.060 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE id > 9;
+----+---------------+----------+---------------------------------------+----------+
| id | title         | length   | genre                                 | album_id |
+----+---------------+----------+---------------------------------------+----------+
| 10 | Born This Way | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry         | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend     | 00:02:51 | Pop                                   |       18 |
| 13 | Into You      | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next  | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic     | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost          | 00:02:32 | Pop                                   |       22 |
+----+---------------+----------+---------------------------------------+----------+
7 rows in set (0.340 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;
+----+---------------+----------+-------------+----------+
| id | title         | length   | genre       | album_id |
+----+---------------+----------+-------------+----------+
|  1 | Gangnam Style | 00:02:53 | K-Pop       |        1 |
|  3 | Kisapmata     | 00:03:19 | OPM         |        2 |
|  5 | Love Story    | 00:02:13 | Country pop |       13 |
+----+---------------+----------+-------------+----------+
3 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");
+----+---------------+----------+-------+----------+
| id | title         | length   | genre | album_id |
+----+---------------+----------+-------+----------+
|  1 | Gangnam Style | 00:02:53 | K-Pop |        1 |
| 12 | Boyfriend     | 00:02:51 | Pop   |       18 |
| 16 | Lost          | 00:02:32 | Pop   |       22 |
+----+---------------+----------+-------+----------+
3 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title FROM songs WHERE title LIKE "%e";
+----------------+
| title          |
+----------------+
| Gangnam Style  |
| State of Grace |
+----------------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title FROM songs WHERE title LIKE "%y";
+---------------+
| title         |
+---------------+
| Love Story    |
| Born This Way |
| Sorry         |
+---------------+
3 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title FROM songs WHERE title LIKE "Bo%";
+---------------+
| title         |
+---------------+
| Born This Way |
| Boyfriend     |
+---------------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title FROM songs WHERE title LIKE "%st%";
+----------------+
| title          |
+----------------+
| Gangnam Style  |
| Love Story     |
| State of Grace |
| Lost           |
+----------------+
4 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title FROM songs ORDER BY title ASC;
+----------------+
| title          |
+----------------+
| 24K Magic      |
| Black Eyes     |
| Born This Way  |
| Boyfriend      |
| Fearless       |
| Gangnam Style  |
| Into You       |
| Kisapmata      |
| Kundiman       |
| Lost           |
| Love Story     |
| Red            |
| Shallow        |
| Sorry          |
| State of Grace |
| Thank U Next   |
+----------------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title FROM songs ORDER BY title DESC;
+----------------+
| title          |
+----------------+
| Thank U Next   |
| State of Grace |
| Sorry          |
| Shallow        |
| Red            |
| Love Story     |
| Lost           |
| Kundiman       |
| Kisapmata      |
| Into You       |
| Gangnam Style  |
| Fearless       |
| Boyfriend      |
| Born This Way  |
| Black Eyes     |
| 24K Magic      |
+----------------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT DISTINCT genre FROM songs;
+---------------------------------------+
| genre                                 |
+---------------------------------------+
| K-Pop                                 |
| OPM                                   |
| Pop rock                              |
| Country pop                           |
| Alternative Rock                      |
| Country                               |
| Rock and roll                         |
| Country, rock, folk rock              |
| Electropop                            |
| Dancehall-poptropical housemoombahton |
| Pop                                   |
| EDM house                             |
| Pop, R&B                              |
| Funk, disco, R&B                      |
+---------------------------------------+
14 rows in set (0.001 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.002 sec)

MariaDB [music_db]> SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;
+----+---------------+----+-----------------+------------+-----------+
| id | name          | id | name            | year       | artist_id |
+----+---------------+----+-----------------+------------+-----------+
|  1 | Rivermaya     |  2 | Trip            | 1996-01-01 |         1 |
|  2 | Psy           |  1 | Psy 6           | 2012-01-01 |         2 |
|  3 | Taylor Swift  | 13 | Fearless        | 2008-01-01 |         3 |
|  3 | Taylor Swift  | 14 | Red             | 2012-02-02 |         3 |
|  4 | Lady  Gaga    | 15 | A Star Is Born  | 2018-03-03 |         4 |
|  4 | Lady  Gaga    | 16 | Born This Way   | 2011-04-04 |         4 |
|  5 | Justin Bieber | 17 | Purpose         | 2015-05-05 |         5 |
|  5 | Justin Bieber | 18 | Believe         | 2012-06-06 |         5 |
|  6 | Ariana Grande | 19 | Dangerous Woman | 2016-07-07 |         6 |
|  6 | Ariana Grande | 20 | Thank U, Next   | 2019-08-08 |         6 |
|  7 | Bruno Mars    | 21 | 24K Magic       | 2016-09-09 |         7 |
|  7 | Bruno Mars    | 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+---------------+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums JOIN artists ON albums.artist_id = artists.id;
+----+-----------------+------------+-----------+----+---------------+
| id | name            | year       | artist_id | id | name          |
+----+-----------------+------------+-----------+----+---------------+
|  2 | Trip            | 1996-01-01 |         1 |  1 | Rivermaya     |
|  1 | Psy 6           | 2012-01-01 |         2 |  2 | Psy           |
| 13 | Fearless        | 2008-01-01 |         3 |  3 | Taylor Swift  |
| 14 | Red             | 2012-02-02 |         3 |  3 | Taylor Swift  |
| 15 | A Star Is Born  | 2018-03-03 |         4 |  4 | Lady  Gaga    |
| 16 | Born This Way   | 2011-04-04 |         4 |  4 | Lady  Gaga    |
| 17 | Purpose         | 2015-05-05 |         5 |  5 | Justin Bieber |
| 18 | Believe         | 2012-06-06 |         5 |  5 | Justin Bieber |
| 19 | Dangerous Woman | 2016-07-07 |         6 |  6 | Ariana Grande |
| 20 | Thank U, Next   | 2019-08-08 |         6 |  6 | Ariana Grande |
| 21 | 24K Magic       | 2016-09-09 |         7 |  7 | Bruno Mars    |
| 22 | Earth to Mars   | 2011-10-10 |         7 |  7 | Bruno Mars    |
+----+-----------------+------------+-----------+----+---------------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM playlists_songs;
Empty set (0.230 sec)

MariaDB [music_db]> SELECT * FROM albums JOIN artists ON artists.id = albums.artist_id;
+----+-----------------+------------+-----------+----+---------------+
| id | name            | year       | artist_id | id | name          |
+----+-----------------+------------+-----------+----+---------------+
|  2 | Trip            | 1996-01-01 |         1 |  1 | Rivermaya     |
|  1 | Psy 6           | 2012-01-01 |         2 |  2 | Psy           |
| 13 | Fearless        | 2008-01-01 |         3 |  3 | Taylor Swift  |
| 14 | Red             | 2012-02-02 |         3 |  3 | Taylor Swift  |
| 15 | A Star Is Born  | 2018-03-03 |         4 |  4 | Lady  Gaga    |
| 16 | Born This Way   | 2011-04-04 |         4 |  4 | Lady  Gaga    |
| 17 | Purpose         | 2015-05-05 |         5 |  5 | Justin Bieber |
| 18 | Believe         | 2012-06-06 |         5 |  5 | Justin Bieber |
| 19 | Dangerous Woman | 2016-07-07 |         6 |  6 | Ariana Grande |
| 20 | Thank U, Next   | 2019-08-08 |         6 |  6 | Ariana Grande |
| 21 | 24K Magic       | 2016-09-09 |         7 |  7 | Bruno Mars    |
| 22 | Earth to Mars   | 2011-10-10 |         7 |  7 | Bruno Mars    |
+----+-----------------+------------+-----------+----+---------------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
+----+---------------+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
| id | name          | id | name            | year       | artist_id | id | title          | length   | genre                                 | album_id |
+----+---------------+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
|  1 | Rivermaya     |  2 | Trip            | 1996-01-01 |         1 |  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  1 | Rivermaya     |  2 | Trip            | 1996-01-01 |         1 |  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  2 | Psy           |  1 | Psy 6           | 2012-01-01 |         2 |  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  3 | Taylor Swift  | 13 | Fearless        | 2008-01-01 |         3 |  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  3 | Taylor Swift  | 13 | Fearless        | 2008-01-01 |         3 |  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  3 | Taylor Swift  | 14 | Red             | 2012-02-02 |         3 |  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  3 | Taylor Swift  | 14 | Red             | 2012-02-02 |         3 |  7 | Red            | 00:02:04 | Country                               |       14 |
|  4 | Lady  Gaga    | 15 | A Star Is Born  | 2018-03-03 |         4 |  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  4 | Lady  Gaga    | 15 | A Star Is Born  | 2018-03-03 |         4 |  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
|  4 | Lady  Gaga    | 16 | Born This Way   | 2011-04-04 |         4 | 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
|  5 | Justin Bieber | 17 | Purpose         | 2015-05-05 |         5 | 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
|  5 | Justin Bieber | 18 | Believe         | 2012-06-06 |         5 | 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
|  6 | Ariana Grande | 19 | Dangerous Woman | 2016-07-07 |         6 | 13 | Into You       | 00:02:42 | EDM house                             |       19 |
|  6 | Ariana Grande | 20 | Thank U, Next   | 2019-08-08 |         6 | 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
|  7 | Bruno Mars    | 21 | 24K Magic       | 2016-09-09 |         7 | 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
|  7 | Bruno Mars    | 22 | Earth to Mars   | 2011-10-10 |         7 | 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+---------------+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.002 sec)

MariaDB [music_db]> SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;
+----+---------------+----+-----------------+------------+-----------+
| id | name          | id | name            | year       | artist_id |
+----+---------------+----+-----------------+------------+-----------+
|  1 | Rivermaya     |  2 | Trip            | 1996-01-01 |         1 |
|  2 | Psy           |  1 | Psy 6           | 2012-01-01 |         2 |
|  3 | Taylor Swift  | 13 | Fearless        | 2008-01-01 |         3 |
|  3 | Taylor Swift  | 14 | Red             | 2012-02-02 |         3 |
|  4 | Lady  Gaga    | 15 | A Star Is Born  | 2018-03-03 |         4 |
|  4 | Lady  Gaga    | 16 | Born This Way   | 2011-04-04 |         4 |
|  5 | Justin Bieber | 17 | Purpose         | 2015-05-05 |         5 |
|  5 | Justin Bieber | 18 | Believe         | 2012-06-06 |         5 |
|  6 | Ariana Grande | 19 | Dangerous Woman | 2016-07-07 |         6 |
|  6 | Ariana Grande | 20 | Thank U, Next   | 2019-08-08 |         6 |
|  7 | Bruno Mars    | 21 | 24K Magic       | 2016-09-09 |         7 |
|  7 | Bruno Mars    | 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+---------------+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT artists.name, albums.name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| name          | name            |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT artists.name, albums.name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| name          | name            |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.001 sec)

Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 145
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> SELECT artists.name, albums.name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| name          | name            |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.002 sec)

MariaDB [music_db]> SELECT artists.name, albums.name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| name          | name            |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT artists.name AS artists_name, albums.name AS albums_name
    -> FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| artists_name  | albums_name     |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.076 sec)

MariaDB [music_db]> SELECT artists.name AS artists_name, albums.name AS albums_name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| artists_name  | albums_name     |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT artists.name, albums.name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| name          | name            |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.002 sec)

MariaDB [music_db]> SELECT artists.name AS artists_name, albums.name AS albums_name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| artists_name  | albums_name     |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.042 sec)

MariaDB [music_db]>
